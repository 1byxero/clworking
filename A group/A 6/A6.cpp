#include <iostream>
/*#include <fstream>
#include <sstream>*/
#include <vector>
//#include <string>
//#include <stdlib.h>
#include <math.h>
using namespace std;
/*vector<string> split(string str,char delimiter)
{
	vector<string> internal;
	stringstream ss(str);
	string tok;
	while(getline(ss,tok,delimiter))
	{
		internal.push_back(tok);
	}
	return internal;
}*/
int main()
{
	int n;
	cout<<"Enter no. of data items:"<<endl;
	cin>>n;
	double x[n],y[n];
	cout<<"entler values"<<endl;
	for(int i=0;i<n;i++)
	{
		cin>>x[i]>>y[i];
	}
	//ifstream in("ip.csv");
	//ofstream cout;
	//string s;
	vector<string> p;
	
	/*int lol=0;
	while(in>>s)
	{
		p=  split(s,',');
		x[lol]=atof(p[0].c_str());
		y[lol]=atof(p[1].c_str());
		lol++;
	}*/

	int k;
	cout<<"Enter number of clusters:";
	cin>>k;
	vector<double> *mean;
	mean=new vector<double>[2];
	cout<<"Enter cluster means:\n";
	for(int i=0;i<k;i++)
	{
		int mx,my;
		cin>>mx>>my;
		mean[0].push_back(mx);
		mean[1].push_back(my);
	}

	vector<double> *omean;
	omean=new vector<double>[2];
	//stores position of data
	vector<int> *c;		//k clusters
	c=new vector<int>[n];	//each cluster of size n
	int turn =0;
	while(1)
	{
		cout<<"Iteration number "<<turn<<endl;
		for(int i=0;i<2;i++)
			omean[i]=mean[i];
		for(int i=0;i<k;i++)
			c[i].erase(c[i].begin(),c[i].end());
		
		for(int i=0;i<n;i++)  //for every point
		{
			double min=99999;
			int pos;
			for(int j=0;j<k;j++) //for every cluster ...checks every point fits in which cluster
			{
				double temp=sqrt(pow((x[i]-mean[0][j]),2)+pow((y[i]-mean[1][j]),2));
				if(temp<min)
				{
					min=temp;
					pos=j;
				}
			}
			c[pos].push_back(i);             
		}

		for(int i=0;i<k;i++)  //for every cluster
		{
			double xsum=0,ysum=0;
			for(int j=0;j<c[i].size();j++)   //for every point in cluster
			{
				xsum+=x[c[i][j]];
				ysum+=y[c[i][j]];

			}
			mean[0][i]=xsum/c[i].size();
			mean[1][i]=ysum/c[i].size();
		}
		
		turn++;
		if(omean[0]==mean[0] && omean[1]==mean[1])
			break;
	}

	//cout.open("op.csv");
	for(int i=0;i<k;i++)
	{
		cout<<"Cluster "<<i+1<<endl;
		cout<<"Mean of cluster\n";
		cout<<mean[0][i]<<","<<mean[1][i]<<endl;
		cout<<"Cluster data\n";

		for(int j=0;j<c[i].size();j++)
		{
			int pos=c[i][j];		
			cout<<x[pos]<<","<<y[pos]<<endl;
		}
		cout<<endl;	
	}
	
	//in.close();
	//cout.close();
	
	return 0;
}

/*Output:
Cluster 1	
Mean of cluster	
271	640.5
Cluster data	
30	716
488	688
298	504
359	459
135	696
316	780
	
Cluster 2	
Mean of cluster	
507	117.333
Cluster data	
377	129
809	207
335	16
	
Cluster 3	
Mean of cluster	
175.8	188.8
Cluster data	
113	93
292	291
163	251
202	182
109	127
	
Cluster 4	
Mean of cluster	
215	916.6
Cluster data	
246	825
362	976
135	967
65	965
267	850
	
Cluster 5	
Mean of cluster	
827.455	709.455
Cluster data	
726	905
870	493
744	851
987	921
976	910
925	606
694	709
749	606
897	300
697	709
837	794*/

